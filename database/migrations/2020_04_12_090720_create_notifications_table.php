<?php
use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        Schema::create(
            'notifications',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->id()->unsigned();
                $table->string('subject', 255)->nullable();
                $table->text('message', 65535);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() : void
    {
        Schema::dropIfExists('notifications');
    }
}
