<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) : array
    {
        return [
            "id" => $this->id,
            "subject" => $this->subject,
            "message" => $this->message,
            "created_at" => strtotime($this->created_at)
        ];
    }
}
