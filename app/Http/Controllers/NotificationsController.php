<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use App\Notification as NotificationModel;

use App\Http\Requests\StoreNotificationRequest;

use App\Http\Resources\NotificationsResource;

use Aws\Sns\SnsClient;

use Aws\Exception\AWSException;

use Log;
use Exception;

class NotificationsController extends Controller
{
    private $snsClient;

    public function __construct()
    {
        $this->snsClient = new SnsClient(
            [
                'profile' => 'default',
                'region' => 'us-east-2',
                'version' => '2010-03-31'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() : JsonResponse
    {
        $notifications = NotificationModel::orderBy('id', 'desc')->get();

        return response()->json(NotificationsResource::collection($notifications), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreNotificationRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreNotificationRequest $request) : JsonResponse
    {
        try {
            $data = $request->all();

            $topic = $this->snsClient->listTopics()["Topics"][0];

            $this->snsClient->publish(
                [
                    "Subject" => $data["subject"],
                    "Message" => $data["message"],
                    "TopicArn" => $topic["TopicArn"]
                ]
            );

            NotificationModel::create($data);

            $jsonResponse = [
                "message" => "Notificación creada exitosamente!"
            ];

            $statusCode = 200;
        } catch (AwsException | Exception $e) {
            Log::error($e->getMessage());

            $jsonResponse = [
                "message" => $e->getMessage()
            ];

            $statusCode = 500;
        } finally {
            return response()->json($jsonResponse, $statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notification as NotificationModel  $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(NotificationModel $notification) : JsonResponse
    {
        return response()->json(new NotificationsResource($notification), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notification as NotificationModel  $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(NotificationModel $notification) : JsonResponse
    {
        try {
            $notification->delete();

            $jsonResponse = [
                "message" => "Notificación eliminada con éxito!"
            ];

            $statusCode = 200;
        } catch (Exception $e) {
            $jsonResponse = [
                "message" => "Se ha producido un error al eliminar la notificación!"
            ];

            $statusCode = 500;
        } finally {
            return response()->json($jsonResponse, $statusCode);
        }
    }
}
